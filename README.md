Calculator widget app

This app has addition, subtraction, multiplication and division functions.
You can type in expressions with one or more operators. Use buttons in provided window to type in expression you want.
You can pop some of previous results by double clicking it.
Type in numbers with exponential using "e" button like shown here: 1e5, 1.0e-2.

To use this app you need to have installed QtCreator with correctly set up desktop kit.
