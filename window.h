#ifndef WINDOW_H
#define WINDOW_H

#include <QWidget>
#include <QPushButton>
#include <QLineEdit>
#include <QListWidget>

class Window : public QWidget
{
    Q_OBJECT
public:
    explicit Window(QWidget *parent = nullptr);

signals:

private:
    QLineEdit *input;
    QPushButton *digits[10];
    QPushButton *dot;
    QPushButton *e;
    QPushButton *signsButtons[4];
    QPushButton *equal;
    QPushButton *c;
    QPushButton *del;
    QListWidget *historyWidget;

    QList<QString> numbers;
    QList<int> signs;

    QList<int> priority;

    bool isResult = false;
    QString result();
    void renewCalc();

    QString errorOccured();

private slots:
    void clickButton1();
    void clickButton2();
    void clickButton3();
    void clickButton4();
    void clickButton5();
    void clickButton6();
    void clickButton7();
    void clickButton8();
    void clickButton9();
    void clickButton0();

    void clickButtonDot();
    void clickButtonE();

    void clickPlus();
    void clickMinus();
    void clickMultiply();
    void clickDivide();

    void clickEqual();

    void clickC();
    void clickDel();

    void popHistory(QListWidgetItem *item);
};

#endif // WINDOW_H
