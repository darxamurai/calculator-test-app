#include "window.h"

#include <QDebug>
#include <QString>

const QString signstr[] = {"+", "-", "*", "/"};

Window::Window(QWidget *parent) : QWidget(parent)
{
    numbers.append("");

    setFixedSize(300,400);

    input = new QLineEdit (this);
    input->setGeometry(0,25,400,50);
    input->setReadOnly(true);


    void (Window::*clickButton[])() = {&Window::clickButton1, &Window::clickButton2, &Window::clickButton3, &Window::clickButton4, &Window::clickButton5, &Window::clickButton6, &Window::clickButton7, &Window::clickButton8, &Window::clickButton9, &Window::clickButton0};

    for (int i = 0; i<10; ++i){
        digits[i] = new QPushButton(this);
        QString text = QString::number((i+1)%10);
        digits[i]->setText(text);
        digits[i]->setGeometry(50*(i%3),100+50*(i/3),50,50);

        connect(digits[i], &QPushButton::released, this, clickButton[i]);
    }


    dot = new QPushButton(this);
    dot->setText(".");
    dot->setGeometry(50, 250, 50, 50);

    connect(dot, &QPushButton::released, this, &Window::clickButtonDot);


    e = new QPushButton(this);
    e->setText("e");
    e->setGeometry(100, 250, 50, 50);

    connect(e, &QPushButton::released, this, &Window::clickButtonE);


    void (Window::*clickSign[])() = {&Window::clickPlus, &Window::clickMinus, &Window::clickMultiply, &Window::clickDivide};

    for (int i = 0; i<4; ++i){
        signsButtons[i] = new QPushButton(this);
        signsButtons[i]->setGeometry(200 + 50*(i%2),100+50*(i/2),50,50);
        signsButtons[i]->setText(signstr[i]);

        connect(signsButtons[i], &QPushButton::released, this, clickSign[i]);
    }


    equal = new QPushButton(this);
    equal->setText("=");
    equal->setGeometry(200, 200, 75, 75);

    connect(equal, &QPushButton::released, this, &Window::clickEqual);


    c = new QPushButton(this);
    c->setText("C");
    c->setGeometry(0, 350, 50, 50);

    connect(c, &QPushButton::released, this, &Window::clickC);


    del = new QPushButton(this);
    del->setText("<-");
    del->setGeometry(50, 350, 50, 50);

    connect(del, &QPushButton::released, this, &Window::clickDel);


    historyWidget = new QListWidget(this);
    historyWidget->setGeometry(150, 300, 150, 100);

    connect(historyWidget, &QListWidget::itemDoubleClicked, this, &Window::popHistory);
}

QString Window::result(){

    while (!signs.isEmpty()){
        int i = 0;
        if (!priority.isEmpty()){
            i = priority[0];
            priority.removeAt(0);
            for (int k = 0; k < priority.size(); ++k){
                priority[k]--;
            }
        }

        auto num1 = numbers[i].contains('.') || numbers[i].contains('e') ? numbers[i].toDouble() : numbers[i].toLongLong();
        if (numbers[i] != "0" && num1 == 0){                    // check if parsed correctly
            return errorOccured();
        }

        if (numbers[1 + i].isEmpty()){                          // check if second number exists
            return errorOccured();
        }

        auto num2 = numbers[1 + i].contains('.') || numbers[i + 1].contains('e') ? numbers[1 + i].toDouble() : numbers[1 + i].toLongLong();
        if (numbers[1 + i] != "0" && num2 == 0){                   // check if parsed correctly
            return errorOccured();
        }


        switch (signs[i]){
        case 0:
            numbers[1 + i] = numbers[i].number(num1+num2);
            break;
        case 1:
            numbers[1 + i] = numbers[i].number(num1-num2);
            break;
        case 2:
            numbers[1 + i] = numbers[i].number(num1*num2);
            break;
        case 3:
            if (num2 == 0){
                return errorOccured();
            }
            numbers[1 + i] = numbers[i].number(num1/num2);
            break;
        default:
            return errorOccured();
        }
        signs.removeAt(i);
        numbers.removeAt(i);

    }

    return numbers[numbers.size()-1];
}

void Window::clickButton1(){
    numbers[numbers.size()-1].append("1");
    input->insert("1");
}

void Window::clickButton2(){
    numbers[numbers.size()-1].append("2");
    input->insert("2");
}

void Window::clickButton3(){
    numbers[numbers.size()-1].append("3");
    input->insert("3");
}

void Window::clickButton4(){
    numbers[numbers.size()-1].append("4");
    input->insert("4");
}

void Window::clickButton5(){
    numbers[numbers.size()-1].append("5");
    input->insert("5");
}

void Window::clickButton6(){
    numbers[numbers.size()-1].append("6");
    input->insert("6");
}

void Window::clickButton7(){
    numbers[numbers.size()-1].append("7");
    input->insert("7");
}

void Window::clickButton8(){
    numbers[numbers.size()-1].append("8");
    input->insert("8");
}

void Window::clickButton9(){
    numbers[numbers.size()-1].append("9");
    input->insert("9");
}

void Window::clickButton0(){
    numbers[numbers.size()-1].append("0");
    input->insert("0");
}

void Window::clickButtonDot(){
    if (!numbers[numbers.size()-1].contains(".") && !numbers[numbers.size()-1].contains("e")){
        numbers[numbers.size()-1].append(".");
        input->insert(".");
    }
}

void Window::clickButtonE()
{
    if (!numbers[numbers.size()-1].contains("e") && numbers[numbers.size()-1] != "" && numbers[numbers.size()-1].last(1) != "."){
        numbers[numbers.size()-1].append("e");
        input->insert("e");
    }
}

void Window::clickPlus(){

    if (numbers[numbers.size()-1] != "" && numbers[numbers.size()-1].last(1) == "e"){
        input->insert("+");

        numbers[numbers.size()-1].append("+");
    } else if (numbers.size() - 1 == signs.size() && numbers[numbers.size()-1] != ""  && numbers[numbers.size()-1] != "-"){
        input->insert("+");

        signs.append(0);
        numbers.append("");
    }
}

void Window::clickMinus(){

    if (numbers[numbers.size()-1] == "" || numbers[numbers.size()-1].last(1) == "e"){
        input->insert("-");

        numbers[numbers.size()-1].append("-");
    } else if (numbers.size() - 1 == signs.size() && numbers[numbers.size()-1] != ""  && numbers[numbers.size()-1] != "-"){
        input->insert("-");

        signs.append(1);
        numbers.append("");
    }
}

void Window::clickMultiply(){ 

    if (numbers.size() - 1 == signs.size() && numbers[numbers.size()-1] != "" && numbers[numbers.size()-1] != "-"){
        input->insert("*");
        signs.append(2);
        priority.append(signs.size()-1);

        numbers.append("");
    }
}

void Window::clickDivide(){

    if (numbers.size() - 1 == signs.size() && numbers[numbers.size()-1] != "" && numbers[numbers.size()-1] != "-"){
        input->insert("/");
        signs.append(3);
        priority.append(signs.size()-1);

        numbers.append("");
    }
}

void Window::clickEqual(){
    QListWidgetItem *item = new QListWidgetItem(input->text());
    historyWidget->insertItem(0,item);
    input->setText(result());
    item->setText(item->text() + " = " + input->text());

    isResult = true;
}

void Window::clickC(){
    input->clear();
    renewCalc();
}

void Window::clickDel(){
    if (numbers[0] != ""){
        QChar c = input->text().back();
        QString number = numbers[numbers.size()-1];
        number.chop(1);

        if (c.isDigit() || c == '.' || c == 'e'){
            numbers[numbers.size()-1].chop(1);
        } else if (number.size() > 0 && number.last(1) == 'e'){
            numbers[numbers.size()-1].chop(1);
        }
        else {
            if (!signs.isEmpty()){
                signs.removeLast();
            }
            if (!numbers.isEmpty())
            {
                numbers.removeLast();
            }
        }
        input->backspace();
    }

}

void Window::popHistory(QListWidgetItem *item){
    numbers[0] = item->text().split(" ")[2];
    input->setText(numbers[0]);
}

void Window::renewCalc(){
    signs.clear();
    numbers.clear();
    numbers.append("");
}

QString Window::errorOccured()
{
    renewCalc();
    return "ERROR";
}
